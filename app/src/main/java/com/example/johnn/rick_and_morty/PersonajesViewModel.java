package com.example.johnn.rick_and_morty;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.example.johnn.rick_and_morty.AppDatabase;
import com.example.johnn.rick_and_morty.Personajes;
import com.example.johnn.rick_and_morty.PersonjesDao;

import java.util.ArrayList;
import java.util.List;

public abstract class PersonajesViewModel extends AndroidViewModel {

    private final Application app;
    private final AppDatabase appDatabase;
    private final PersonjesDao personajesDao;
    private LiveData<List<Personajes>> personajes;

    public PersonajesViewModel(Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication());
        this.personajesDao = appDatabase.getPersonajes();
    }

    public LiveData<List<Personajes>> getPersonajes() {
        return personajesDao.getPersonajes();
    }


    public void reload() {
        // do async operation to fetch users
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Personajes>> {
        @Override
        protected ArrayList<Personajes> doInBackground(Void... voids) {

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(
                    app.getApplicationContext()
            );

            String personajes_nombre = preferences.getString("pais", "");
            String tipo_consulta = preferences.getString("tipus_consulta", "");

            PersonajesAPI api = new PersonajesAPI();
            ArrayList<Personajes> result;

            //modificar esta parte
            if(personajes_nombre==""){
                if (tipo_consulta != "") {
                    result = api.PersonajesEspecies(tipo_consulta);
                } else {
                    result = api.TodosLosPersonajes();
                }
            }
            else{
                result=api.PersonajesNombre(personajes_nombre);
            }

            personajesDao.deletePersonajes();
            personajesDao.addPersonajes(result);

            return result;
        }

    }




}
