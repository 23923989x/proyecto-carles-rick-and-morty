package com.example.johnn.rick_and_morty;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.johnn.rick_and_morty.databinding.FragmentDetailBinding;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    private View view;

    private FragmentDetailBinding binding;

    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentDetailBinding.inflate(inflater);
        view = binding.getRoot();

        Intent i = getActivity().getIntent();


        if (i != null) {
            Personajes personaje = (Personajes) i.getSerializableExtra("Personaje");

            if (personaje != null) {
                updateUi(personaje);
            }
        }
        //parte de fragment tablet


        SharedViewModel sharedModel = ViewModelProviders.of(
                getActivity()
        ).get(SharedViewModel.class);
        sharedModel.getSelected().observe(this, new Observer<Personajes>() {
            @Override
            public void onChanged(@Nullable Personajes personajes) {
                updateUi(personajes);
            }
        });

//hasta aqui


        return view;
    }

    private void updateUi(Personajes personaje) {
        //Log.d("Personaje", personaje.toString());


        binding.nameperson2.setText(personaje.getName());
        binding.statusPerson2.setText(personaje.getStatus());
        binding.speciesPerson2.setText(personaje.getSpecies());

        Glide.with(getContext()).load(
                 personaje.getImage()
        ).into(binding.image2);



    }
}
