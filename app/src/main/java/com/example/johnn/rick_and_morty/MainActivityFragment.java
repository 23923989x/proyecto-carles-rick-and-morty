package com.example.johnn.rick_and_morty;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.johnn.rick_and_morty.databinding.FragmentMainBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {


    private ArrayList<Personajes> items;
    private PersonajesAdapter  adapter;
    private FragmentMainBinding binding;
    private SharedPreferences preferences;
    private  PersonajesViewModel model;
    private SharedViewModel sharedModel;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMainBinding.inflate(inflater);
        View view = binding.getRoot();

        items = new ArrayList<>();
        adapter = new PersonajesAdapter(
                getContext(),
                R.layout.personaje1,
                items

        );

        sharedModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        binding.lvPelis.setAdapter(adapter);

        binding.lvPelis.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Personajes personaje = (Personajes) adapterView.getItemAtPosition(i);

                if (!esTablet()) {
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("Personaje", personaje);
                    startActivity(intent);
                }
                else {
                    sharedModel.select(personaje);
                }
            }
        });

        //parte nueva de la base de datos pero este metodo no funciona
/*
        model = ViewModelProviders.of(this).get(PersonajesViewModel.class);
        model.getPersonajes().observe(this, new Observer<List<Personajes>>() {
            @Override
            public void onChanged(@Nullable List<Personajes> personajes) {
                adapter.clear();
                adapter.addAll(personajes);
            }
        });

*/

        return view;
    }

    boolean esTablet() {
        return getResources().getBoolean(R.bool.tablet);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_refresh, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();

        if(id == R.id.refresh){
            Refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        Refresh();
    }

    private void Refresh(){

        RefreshDataTask task = new RefreshDataTask();
        task.execute();
        //parte de base de datos..el model no funciona. da error
        //model.reload();
    }


//esta parte hay que modificarla para que se adecue a el array de la api

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Personajes>> {

        @Override
        protected ArrayList<Personajes> doInBackground(Void... voids) {
            PersonajesAPI api = new PersonajesAPI();


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String personajes_nombre = preferences.getString("pais", "");
        String tipo_consulta = preferences.getString("tipus_consulta", "");


            ArrayList<Personajes> result;

            if(personajes_nombre==""){
                if (tipo_consulta != "") {
                result = api.PersonajesEspecies(tipo_consulta);
                } else {
                result = api.TodosLosPersonajes();
                }
            }
            else{
                result=api.PersonajesNombre(personajes_nombre);
            }

            //aqui pasan los datos de la api y los reproduce en el run
            //Log.d("DEBUG", result.toString());

            return result;

        }

        @Override
        protected void onPostExecute(ArrayList<Personajes> persona) {
            adapter.clear();
            for (Personajes peli : persona) {
                adapter.add(peli);
            }

        }

    }

}
