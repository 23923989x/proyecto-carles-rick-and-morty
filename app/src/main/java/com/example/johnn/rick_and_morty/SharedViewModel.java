package com.example.johnn.rick_and_morty;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<Personajes> selected = new MutableLiveData<Personajes>();

    public void select(Personajes personaje){
        selected.setValue(personaje);
    }

    public LiveData<Personajes> getSelected() {
        return selected;
    }


}
