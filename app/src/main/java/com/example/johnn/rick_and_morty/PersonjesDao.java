package com.example.johnn.rick_and_morty;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PersonjesDao {



    @Query("select * from Personajes")
    LiveData<List<Personajes>> getPersonajes();

    @Insert
    void addPeronaje(Personajes personajes);

    @Insert
    void addPersonajes(List<Personajes> personajes);

    @Delete
    void deletePersonaje(Personajes personajes);

    @Query("DELETE FROM Personajes")
    void deletePersonajes();





}
