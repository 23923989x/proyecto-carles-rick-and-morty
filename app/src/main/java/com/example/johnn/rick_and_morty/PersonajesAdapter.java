package com.example.johnn.rick_and_morty;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.johnn.rick_and_morty.databinding.Personaje1Binding;

import java.util.List;

public class PersonajesAdapter extends ArrayAdapter<Personajes> {

    public PersonajesAdapter(Context context, int resource, List<Personajes> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Obtenim l'objecte en la possició corresponent
        Personajes personaje = getItem(position);
        //Log.w("XXXX", personaje.toString());

        Personaje1Binding binding=null;

        // Mirem a veure si la View s'està reusant, si no es així "inflem" la View
        // https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView#row-view-recycling
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater,R.layout.personaje1, parent, false);
        }else {
            binding = DataBindingUtil.getBinding(convertView);
        }

            binding.nameperson.setText(personaje.getName());
        binding.statusPerson.setText(personaje.getStatus());
        binding.speciesPerson.setText(personaje.getSpecies());



        Glide.with(getContext()).load(
                 personaje.getImage()
        ).into(binding.image2);



        return binding.getRoot();
    }

}