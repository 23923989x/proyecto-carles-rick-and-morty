package com.example.johnn.rick_and_morty;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class PersonajesAPI {

    private final String BASE_URL = "https://rickandmortyapi.com/api/";


    ArrayList<Personajes> TodosLosPersonajes(){
        Uri builtUri=Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("character")
                .build();
        String url =builtUri.toString();

        return doCall(url);

    }

    ArrayList<Personajes> PersonajesEspecies(String species){
        Uri builtUri=Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("character")
                .appendQueryParameter("species",species)
                .build();
        String url =builtUri.toString();

        return doCall(url);

    }

    ArrayList<Personajes> PersonajesNombre(String Nombre){
        Uri builtUri=Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("character")
                .appendQueryParameter("name",Nombre)
                .build();
        String url =builtUri.toString();

        return doCall(url);

    }


    private ArrayList<Personajes> doCall(String url){
        //parte nueva

        ArrayList<Personajes> personajes = new ArrayList<>();
        try {
            String JsonResponse = HttpUtils.get(url);
            ArrayList<Personajes> list=processJson(JsonResponse);
            personajes.addAll(list);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return personajes;
    }

    private ArrayList<Personajes> processJson(String jsonResponse){
        ArrayList<Personajes> personajes = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonMovies = data.getJSONArray("results");
            for (int i = 0; i < jsonMovies.length(); i++) {
                JSONObject jsonMovie = jsonMovies.getJSONObject(i);

                Personajes personaje = new Personajes();

                personaje.setId(jsonMovie.getInt("id"));
                personaje.setName(jsonMovie.getString("name"));
                personaje.setStatus(jsonMovie.getString("status"));
                personaje.setSpecies(jsonMovie.getString("species"));
                personaje.setImage(jsonMovie.getString("image"));

                personajes.add(personaje);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return personajes;
    }



}
